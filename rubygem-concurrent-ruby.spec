# Generated from concurrent-ruby-1.0.0.gem by gem2rpm -*- rpm-spec -*-
%global gem_name concurrent-ruby

Name: rubygem-%{gem_name}
Version: 1.0.0
Release: 1%{?dist}
Summary: Modern concurrency tools for Ruby
Group: Development/Languages
License: MIT
URL: http://www.concurrent-ruby.com
Source0: https://rubygems.org/gems/%{gem_name}-%{version}.gem
# git clone https://github.com/ruby-concurrency/concurrent-ruby.git && cd concurrent-ruby
# git checkout v1.0.0 && tar czvf concurrent-ruby-1.0.0-specs.tar.gz spec/
Source1: %{gem_name}-%{version}-specs.tar.gz
BuildRequires: ruby(release)
BuildRequires: rubygems-devel
BuildRequires: ruby >= 1.9.3
BuildRequires: rubygem(rspec)
BuildRequires: rubygem(timecop)
BuildArch: noarch

%description
Modern concurrency tools including agents, futures, promises, thread pools,
actors, supervisors, and more.

Inspired by Erlang, Clojure, Go, JavaScript, actors, and classic concurrency
patterns.


%package doc
Summary: Documentation for %{name}
Group: Documentation
Requires: %{name} = %{version}-%{release}
BuildArch: noarch

%description doc
Documentation for %{name}.

%prep
gem unpack %{SOURCE0}

%setup -q -D -T -n  %{gem_name}-%{version}

gem spec %{SOURCE0} -l --ruby > %{gem_name}.gemspec

%build
# Create the gem as gem install only works on a gem file
gem build %{gem_name}.gemspec

# %%gem_install compiles any C extensions and installs the gem into ./%%gem_dir
# by default, so that we can move it into the buildroot in %%install
%gem_install

%install
mkdir -p %{buildroot}%{gem_dir}
cp -a .%{gem_dir}/* \
        %{buildroot}%{gem_dir}/


# Run the test suite
%check
pushd .%{gem_instdir}
tar xzf %{SOURCE1}

# -edge is not part of this gem.
sed -i '/require.*concurrent-edge/ s/^/#/' spec/spec_helper.rb

# We don't have the C extension. This is very likely bug in the test
# implemntation.
sed -i '/allow_c_extensions?/,/^      end/ s/^/#/' spec/concurrent/atomic/atomic_reference_spec.rb

# This is empty file, which fails with recent RSpec, but excluding it,
# no harm is done.
mv spec/concurrent/lazy_register_spec.rb{,.disble}

# Exclude the -edge test cases.
rspec -rspec_helper \
  --exclude-pattern 'spec/concurrent/{actor_spec.rb,channel_spec.rb,lazy_register_spec.rb,channel/**/*,atomic/atomic_markable_reference_spec.rb,edge/**/*}'\
  spec

popd

%files
%dir %{gem_instdir}
%license %{gem_instdir}/LICENSE.txt
%{gem_libdir}
%exclude %{gem_cache}
%{gem_spec}

%files doc
%doc %{gem_docdir}
%doc %{gem_instdir}/CHANGELOG.md
%doc %{gem_instdir}/README.md

%changelog
* Wed Dec 23 2015 Vít Ondruch <vondruch@redhat.com> - 1.0.0-1
- Initial package
